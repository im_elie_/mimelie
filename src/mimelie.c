#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "mimelie.h"
#include "mimelie_db.h"

mime0_t mime0 = MIME0;
mime1_t mime1 = MIME1;
mime2_t mime2 = MIME2;
mime_hm_t mime_hm = MIME_HM;

int hash(char *ext)
{
    /*
        Fonction de hashage basique.

        res := 0
        Pour chaque caractère u :
            Si u est dans le tableau [0-9][A-Z][a-z] :
                k := l'indice de u dans le tableau [0-9][A-Z][a-z]
            Sinon
                k := 63
            res := res + k
            res := (res * HASH_FACTOR) % HM_I
        Renvoyer res

        HASH_FACTOR est le plus petit nombre pour avoir une table de hashage de taille minimum HM_I avec au plus HM_J=4 éléments par ligne.

        Soit n est le nombre de caractères dans l'extension.
        Avec cette fonction de hashage, trouver le code mime d'une extension prend au plus 3n opérations arithmétiques et au plus HM_J=4 comparaisons de chaînes de caractères (4n opérations) donc 7n opérations. C'est beaucoup plus efficace que mes NB_TYPES=739 comparaisons de chaînes de caractères.
    */
    int k;
    int u;
    int res = 0;
    for (long unsigned int i = 0; i < strlen(ext); i++)
    {
        u = (int)ext[i];
        if ((48 <= u) && (u <= 57))
        {
            k = u - 48;
        }
        else if ((65 <= u) && (u <= 90))
        {
            k = u - 65 + 10;
        }
        else if ((97 <= u) && (u <= 122))
        {
            k = u - 97 + 36;
        }
        else
        {
            k = 63;
        }
        res = res + k;
        res = (res * HASH_FACTOR) % HM_I;
    }
    return res;
}

void getMimeTypeFromExt(char *ext, mime_code_t(*codeFromExt))
{
    /*
        Fonction de recherche dans une table de hashage.

        Permet de trouver le code mime associé à une extension en O(1).
    */
    int i = hash(ext);
    int j = 0;
    while (j < 4 && (mime_hm[i][j][0] == -1 || strcmp(mime0[mime_hm[i][j][0]], ext) != 0))
    {
        j++;
    }
    if (j < 4 && strcmp(mime0[mime_hm[i][j][0]], ext) == 0)
    {
        (*codeFromExt)[0] = mime_hm[i][j][0];
        (*codeFromExt)[1] = mime_hm[i][j][1];
        (*codeFromExt)[2] = mime_hm[i][j][2];
    }
    else
    {
        (*codeFromExt)[0] = -1;
        (*codeFromExt)[1] = -1;
        (*codeFromExt)[2] = -1;
    }
}

int getMajorId(char *major)
{
    /*
        Fonction de recherche linéaire dans un tableau de chaînes de caractères.

        Permet de trouver une partie du code mime associé à un type mime en O(m) (m étant le nombre de types mime).
    */
    int major_id = -1;
    for (int i = 0; i < NB_MAJOR_TYPES; i++)
    {
        if (strcmp(major, mime1[i]) == 0)
        {
            major_id = i;
            break;
        }
    }
    return major_id;
}

int getMinorId(char *minor)
{
    /*
        Fonction de recherche linéaire dans un tableau de chaînes de caractères.

        Permet de trouver une partie du code mime associé à un type mime en O(m) (m étant le nombre de types mime).
    */
    int minor_id = -1;
    for (int i = 0; i < NB_MINOR_TYPES; i++)
    {
        if (strcmp(minor, mime2[i]) == 0)
        {
            minor_id = i;
            break;
        }
    }
    return minor_id;
}

void getMimeCodeFromType(char *type, mime_code_t(*codeFromType))
{
    /*
        Fonction de recherche linéaire dans des tableaux de chaînes de caractères.

        Permet de trouver le code mime associé à un type mime en O(m) (m étant le nombre de types mime).
    */

    // On cherche le type majeur (e.g. text)
    char major[S_BUFFER];
    int i = 0;
    while (
        i < S_BUFFER - 1   // On ne sort pas du buffer de major
        && type[i] != '\0' // On ne sort pas du buffer de type
        && type[i] != '/'  // On ne dépasse pas le type mineur
    )
    {
        major[i] = type[i];
        i++;
    }
    major[i] = '\0';
    if (i == S_BUFFER - 1)
    { // Erreur, aucun type majeur n'est aussi long
        (*codeFromType)[0] = -1;
        (*codeFromType)[1] = -1;
        (*codeFromType)[2] = -1;
    }
    else if (type[i] != '\0' || type[i] != '/')
    { // On a lu un supposé type majeur
        int major_id = getMajorId(major);
        if (major_id == -1)
        { // Erreur, ce type majeur n'existe pas
            (*codeFromType)[0] = -1;
            (*codeFromType)[1] = -1;
            (*codeFromType)[2] = -1;
        }
        else
        { // On a reconnu le type
            if (type[i] == '/')
            {        // Il y a (en théorie) un type mineur
                i++; // On passe le / dans le nom du type

                // On cherche le type mineur (e.g. x-java)
                char minor[L_BUFFER];
                int j = 0;
                while (
                    j < L_BUFFER - 1       // On ne sort pas du buffer de minor
                    && type[i + j] != '\0' // On ne sort pas du buffer de type
                )
                {
                    minor[j] = type[i + j];
                    j++;
                }
                minor[j] = '\0';
                if (j == L_BUFFER - 1)
                { // Erreur, aucun type majeur n'est aussi long
                    (*codeFromType)[0] = -1;
                    (*codeFromType)[1] = -1;
                    (*codeFromType)[2] = -1;
                }
                else if (type[i + j] == '\0')
                { // On a lu un supposé type mineur
                    int minor_id = getMinorId(minor);
                    if (minor_id == -1)
                    { // Erreur, ce type mineur n'existe pas
                        (*codeFromType)[0] = -1;
                        (*codeFromType)[1] = -1;
                        (*codeFromType)[2] = -1;
                    }
                    else
                    {
                        (*codeFromType)[0] = -1;
                        (*codeFromType)[1] = major_id;
                        (*codeFromType)[2] = minor_id;
                    }
                }
            }
            else if (type[i] == '\0')
            { // Il n'y a pas de type mineur
                (*codeFromType)[0] = -1;
                (*codeFromType)[1] = major_id;
                (*codeFromType)[2] = -1;
            }
        }
    }
}

bool compareCodes(mime_code_t codeFromExt, mime_code_t codeFromType)
{
    /*
        Fonction de comparaison de codes mime.

        Permet de savoir si une extension est compatible à un type mime.
    */
    // Cas d'un code non reconnu
    if (codeFromExt[0] == -1)
    {
        // L'extension n'a pas été reconnue
        return false;
    }
    else if (codeFromType[1] == -1)
    {
        // Le type n'a pas été reconnu
        return false;
    }
    else
        // Cas d'un code reconnu
        if (codeFromType[2] == -1)
        {
            // Seul le type majeur est précisé
            return codeFromType[1] == codeFromExt[1];
        }
        else
        {
            // Le type majeur et le type mineur sont tous les deux reconnus
            return (codeFromType[2] == codeFromExt[2]) && (codeFromType[1] == codeFromExt[1]);
        }
}

void printMime(mime_code_t codeMime)
{
    /*
        Fonction d'affichage des données associées à un code mime.
    */
    printf("Code mime :     [%d, %d, %d].\n", codeMime[0], codeMime[1], codeMime[2]);
    if (codeMime[0] == -1)
    {
        printf("Extension :     unknown.\n");
    }
    else
    {
        printf("Extension :     %s.\n", mime0[codeMime[0]]);
    }
    if (codeMime[1] == -1)
    {
        printf("Major type :    unknown.\n");
    }
    else
    {
        printf("Major type :    %s.\n", mime1[codeMime[1]]);
    }
    if (codeMime[2] == -1)
    {
        printf("Minor type :    unknown.\n");
    }
    else
    {
        printf("Minor type :    %s.\n", mime2[codeMime[2]]);
    }
}

char *getExt(char *name)
{
    /*
        Fonction qui prend en paramètre un char* (adresse du premier char d'une string) et qui renvoie l'adresse du premier élément après le dernier '.' de cette même string. Si cette string ne contient pas de point alors renvoie l'adresse du premier '\0' de cette string (i.e. fin de cette string).
    */
    char *res = 0;
    int i = 0;
    while (name[i] != '\0')
    {
        if (name[i] == '.')
        {
            res = &(name[i + 1]);
        }
        i++;
    }
    if (res == 0)
    {
        res = &(name[i]);
    }
    return res;
}