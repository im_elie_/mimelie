#include "mimelie.h"
#include <stdio.h>
#include <stdlib.h>

/*
 * Code d'exemple d'utilisation de mimelie.
 *
 * Prend en arguments : un nom de fichier (ou juste une extension : '.zip') et un type (opt.).
 * Affiche les infos obtenus à patir des arguments.
 * Affiche leur compatibilité.
 */

int main(int argc, char **argv)
{
    if (argc < 2 || argc > 3)
    { // Pas le bon nombre d'arguments
        printf("Veuillez entrer un ou deux arguments : mimelie fichier (type).\n");
        exit(EXIT_FAILURE);
    }
    else
    { // DO IT
        // On gère le fichier
        mime_code_t codeFromExt;
        char* ext = getExt(argv[1]);
        getMimeTypeFromExt(ext, &codeFromExt);
        printf("Premier argument : extension.\n");
        printMime(codeFromExt);
        // S'il existe, on gère le type et on compare avec le fichier
        if (argc == 3)
        {
            mime_code_t codeFromType;
            getMimeCodeFromType(argv[2], &codeFromType);
            printf("Second argument : type.\n");
            printMime(codeFromType);
            // On les compare
            if (compareCodes(codeFromExt, codeFromType))
            {
                printf("Les codes sont compatibles.\n");
            }
            else
            {
                printf("Les codes ne sont pas compatibles.\n");
            }
        }
    }
}