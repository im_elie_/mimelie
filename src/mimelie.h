#include <stdbool.h>

#ifndef __MIMELIE__
#define __MIMELIE__

/*
 █████ ██\/██      █/█/█/█/ █/       █/█/█/ █/█/█/█/      __
   █   █\██/█     █/       █/         █/   █/            // \
   █   █ \/ █    █/█/█/   █/         █/   █/█/█/         \\_/ //
   █   █    █   █/       █/         █/   █/             -(||)(')
 █████ █    █  █/█/█/█/ █/█/█/█/ █/█/█/ █/█/█/█/         '''

 --> im_elie_ (Theo@Goureau.eu)
*/

/* ********** ********** ********** **********
 ********** description           **********
 ********** ********** ********** ********** */

/*
 * Cette librairie permet de vérifier l'appartenance d'un fichier à un type mime (e.g. application/x-java)
 * à partir de son extension (e.g. class).
 * 
 * 
 * Les fonctions accessibles sont :
 * 
 * void getMimeTypeFromExt(char *ext, mime_code_t(*codeFromExt))
 * -> Obtient le code mime à partir d'une extention en 0(1).
 * 
 * void getMimeCodeFromType(char *type, mime_code_t(*codeFromType))
 * -> Obtient le code mime à partir d'un type mime en 0(m) où m est le nombre de types mime.
 * 
 * bool compareCodes(mime_code_t codeFromExt, mime_code_t codeFromType)
 * -> Compare deux code pour savoir si une extension est bien d'un type mime.
 * 
 * void printMime(mime_code_t codeMime)
 * -> Affiche les infos (extension et type mime) à partir d'un code mime.
 * 
 * char *getExt(char *name)
 * -> Permet d'obtenir l'extension à partir du nom du fichier.
 * 
 * 
 * Un code mime est un objet du type mime_code_t (aka int [3]) tel que :
 * - mime0[code[0]] est (char*) l'extension (e.g. class) associée.
 * - mime1[code[1]] est (char*) le type majeur (e.g. application) associé.
 * - mime2[code[2]] est (char*) le type mineur (e.g. x-java) associé.
 * - mime_hm[hash[ext]] est le code associé à (char*) l'extension ext (calculé en O(1)).
 */

/* ********** ********** ********** **********
 ********** macros                **********
 ********** ********** ********** ********** */

#define NB_TYPES 739      // Nombre d'extension (e.g. .zip)
#define NB_MAJOR_TYPES 10 // Nombre de grand type mime (e.g. application)
#define NB_MINOR_TYPES 583 // Nombre de petit type mime (e.g. x-java)

#define S_BUFFER 16  // Taille d'un petit buffer (utilisé pour les extension et les grands types)
#define L_BUFFER 128 // Taille d'un grand buffer (utilisé pour les types)

#define HM_I 529        // Nombre de cases dans notre table de hashage
#define HM_J 4          // Nombre maximum d'éléments dans une case de table de hashage
#define HASH_FACTOR 359 // Facteur "magique" dans la fonction de hashage

/* ********** ********** ********** **********
 ********** types                 **********
 ********** ********** ********** ********** */

typedef const char mime0_t[NB_TYPES][S_BUFFER];
typedef const char mime1_t[NB_MAJOR_TYPES][S_BUFFER];
typedef const char mime2_t[NB_MINOR_TYPES][L_BUFFER];
typedef const int mime_hm_t[HM_I][HM_J][3];
typedef int mime_code_t[3];

/* ********** ********** ********** **********
 ********** variables             **********
 ********** ********** ********** ********** */

extern mime0_t mime0;     // Every extension (e.g. class)
extern mime1_t mime1;     // Every major type (e.g. application)
extern mime2_t mime2;     // Every minor type (e.g. x-java)
extern mime_hm_t mime_hm; // Hashmap of codes

/* ********** ********** ********** **********
 ********** functions             **********
 ********** ********** ********** ********** */

/* Ecrit le code mime associé à l'extension ext dans codeFromExt.
    --> Cette fonction est rapide : O(1). */
void getMimeTypeFromExt(char *ext, mime_code_t(*codeFromExt));

/* Ecrit le code mime associé au type mime type dans codeFromType.
    --> Cette fonction est lente : O(n) où n est le nombre de types. */
void getMimeCodeFromType(char *type, mime_code_t(*codeFromType));

/* Renvoie true si l'extension ayant donné codeFromExt est un fichier de type codeFromType.
    --> En cas d'extension inconnue ou de type inconnu, renvoie false. */
bool compareCodes(mime_code_t codeFromExt, mime_code_t codeFromType);

/* Affiche les informations à propos d'un code mime. */
void printMime(mime_code_t codeMime);

/* Obtient l'extension d'une chaine de caractère. */
char *getExt(char *name);

#endif /* __MIMELIE__ */
