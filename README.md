# mimelie
Cette librairie permet de vérifier l'appartenance d'un fichier à un type mime (e.g. `application/x-java`)
à partir de son extension (e.g. `class`).
# Requirments
- git
- gcc
- python3

# Définition
J'ai créé un objet que j'appelle un *code mime*.\
Un code mime est un objet du type `mime_code_t` (aka `int[3]`) tel que :
- `mime0[code[0]]` est (`char*`) l'extension (e.g. `class`) associée.
- `mime1[code[1]]` est (`char*`) le type majeur (e.g. `application`) associé.
- `mime2[code[2]]` est (`char*`) le type mineur (e.g. `x-java`) associé.
- `mime_hm[hash[ext]]` est le code associé à (`char*`) l'extension ext (calculé en O(1)).

# Les fonctions

## Obtenir un code mime
`void getMimeTypeFromExt(char *ext, mime_code_t(*codeFromExt))`\
Obtient le code mime à partir d'une extention en 0(1).

`void getMimeCodeFromType(char *type, mime_code_t(*codeFromType))`\
Obtient le code mime à partir d'un type mime en 0(m) où m est le nombre de types mime.

## Comparer des codes mime
`bool compareCodes(mime_code_t codeFromExt, mime_code_t codeFromType)`\
Compare deux code pour savoir si une extension est bien d'un type mime.

## Afficher des infos
`void printMime(mime_code_t codeMime)`\
Affiche les infos (extension et type mime) à partir d'un code mime.

## Et pour que ce soit plus simple...
`char *getExt(char *name)`\
Permet d'obtenir l'extension à partir du nom du fichier.

# Essayer la librairie
## Clone
```bash
$ git clone https://gitlab.com/im_elie_/mimelie.git
$ cd mimelie
```
## Compile
```bash
$ make
```
## Lancer
```bash
Premier argument : extension.
Code mime :     [242, 0, 210].
Extension :     h.
Major type :    text.
Minor type :    x-chdr.
Second argument : type.
Code mime :     [-1, 0, 210].
Extension :     unknown.
Major type :    text.
Minor type :    x-chdr.
Les codes sont compatibles.
```
Vous pouvez aussi juste lancer avec un nom de fichier en argument et en obtenir le type mime :
```bash
$ ./mimelie mimelie.h
Premier argument : extension.
Code mime :     [242, 0, 210].
Extension :     h.
Major type :    text.
Minor type :    x-chdr.
```
# Editer la base de données des types mime
## La base de données
La base de données est le fichier [mimelie_db](mimelie_db). Il se compose de trois colonnes séparées par des ' > '. La première contient les extensions, la deuxième les types majeurs et la dernière les types mineurs.
## La modifier
Vous pouvez ajouter librement une ligne (l'ordre des lignes n'a pas d'importance). Ensuite vous pouvez recompiler le fichier [mimelie_db.h](src/mimelie_db.h) à l'aide de `make compile_db`. N'oubliez pas de recompiler 
## La vérification des constantes : qu'est-ce que c'est ?
Après avoir modifié la base de données, certaines valeurs (constantes) vont devoir changer avant de recompiler `mimelie` : la vérification des constantes vous conseille pour ces changements. Vous retrouvez les constantes dans les fichiers [constantes.py](gen_c_db/constantes.py) et [mimelie.h](src/mimelie.h). Il faut changer les valeurs **dans les deux fichiers à la fois**.\
NB : Si `HM_J` venait à grandir, essayez de trouver une valeur de `HASH_FACTOR` qui le minimise ;)
# Me contacter
Théo GOUREAU <[Theo@Goureau.eu](mailto:theo@goureau.eu)>
```
 █████ ██\/██      █/█/█/█/ █/       █/█/█/ █/█/█/█/      __
   █   █\██/█     █/       █/         █/   █/            // \
   █   █ \/ █    █/█/█/   █/         █/   █/█/█/         \\_/ //
   █   █    █   █/       █/         █/   █/             -(||)(')
 █████ █    █  █/█/█/█/ █/█/█/█/ █/█/█/ █/█/█/█/         '''

 --> im_elie_ (Theo@Goureau.eu)
```