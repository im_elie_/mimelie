mimelie: build/mimelie.o build/mimelie_exemple.o
	gcc -Wall -Wextra -pedantic -O0 -g3 -fsanitize=address -fno-omit-frame-pointer -fno-optimize-sibling-calls build/mimelie.o build/mimelie_exemple.o -o mimelie

build/:
	mkdir -p build/

build/mimelie.o: src/mimelie.c src/mimelie.h build/
	gcc -c -Wall -Wextra -pedantic -O0 -g3 -fsanitize=address -fno-omit-frame-pointer -fno-optimize-sibling-calls src/mimelie.c -o build/mimelie.o

build/mimelie_exemple.o: src/mimelie_exemple.c build/
	gcc -c -Wall -Wextra -pedantic -O0 -g3 -fsanitize=address -fno-omit-frame-pointer -fno-optimize-sibling-calls src/mimelie_exemple.c -o build/mimelie_exemple.o

compile_db: check_constantes mimelie_db
	python3 gen_c_db/make.py > src/mimelie_db.h

check_constantes:
	python3 gen_c_db/check_constantes.py

clean:
	rm -rf mimelie build/mimelie.o build/mimelie_exemple.o