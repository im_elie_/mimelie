def import_mimelie_db(filename = "mimelie_db") :
    """Renvoie (mime0, mime1, mime2) tableaux de strings contenant les données de la base de données."""
    mime0 = []
    mime1 = []
    mime2 = []

    f_in = open(filename)
    t = f_in.readlines()
    f_in.close()

    for i in range(len(t)) :
        (a, b, c) = t[i].split(" > ")
        mime0.append(a)
        mime1.append(b)
        mime2.append(c[:-1])
    
    return mime0, mime1, mime2