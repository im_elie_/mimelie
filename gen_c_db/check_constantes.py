from constantes import *
from import_mimelie_db import import_mimelie_db
from compress import compress_db
from dicgen import dicgen
import sys

print("[CONSTANTE] Début des tests.")
failed = False

# Le nombre de chaque type

mime0, mime1, mime2 = import_mimelie_db()
n = len(mime0)
(
(mime0_compressed, mime0_codes),
(mime1_compressed, mime1_codes),
(mime2_compressed, mime2_codes)
) = compress_db(mime0, mime1, mime2)

if len(mime0_compressed) != NB_TYPES :
    print("[CONSTANTE] NB_TYPES vaut " + str(NB_TYPES) + ", essayez avec NB_TYPE = " + str(len(mime0_compressed)) + ".")
    failed = True

if len(mime1_compressed) != NB_MAJOR_TYPES :
    print("[CONSTANTE] NB_MAJOR_TYPES vaut " + str(NB_MAJOR_TYPES) + ", essayez avec NB_MAJOR_TYPES = " + str(len(mime1_compressed)) + ".")
    failed = True

if len(mime2_compressed) != NB_MINOR_TYPES :
    print("[CONSTANTE] NB_MINOR_TYPES vaut " + str(NB_MINOR_TYPES) + ", essayez avec NB_MINOR_TYPES = " + str(len(mime2_compressed)) + ".")
    failed = True

# Les buffers
for i in range(n) :
    if len(mime0[i]) +1 > S_BUFFER :
        print("[CONSTANTE] S_BUFFER vaut " + str(S_BUFFER) + ", essayez avec S_BUFFER = " + str(len(mime0[i]) +1) + ".")
        failed = True
        break
    elif len(mime1[i]) +1 > S_BUFFER :
        print("[CONSTANTE] S_BUFFER vaut " + str(S_BUFFER) + ", essayez avec S_BUFFER = " + str(len(mime1[i]) +1) + ".")
        failed = True
        break

for i in range(n) :
    if len(mime1[i]) +1 +len(mime2[i]) +1 > L_BUFFER :
        print("[CONSTANTE] L_BUFFER vaut " + str(L_BUFFER) + ", essayez avec L_BUFFER = " + str(len(mime1[i]) +1 +len(mime2[i]) +1) + ".")
        failed = True
        break

# HM_J
if dicgen(mime0, mime1_codes, mime2_codes) is None :
    failed = True

print("[CONSTANTE] Fin des tests.")

if failed :
    sys.exit(1)