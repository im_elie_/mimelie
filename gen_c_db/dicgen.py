from constantes import *
from hash import hash

def dicgen(mime0, mime1_codes, mime2_codes) :
    res = [
            [
                [-1, -1, -1] for j in range(HM_J)
            ] for i in range(HM_I)
        ]

    for i in range(NB_TYPES) :
        j = hash(mime0[i])
        k = 0
        while res[j][k][0] != -1 :
            k += 1
            if k >= HM_J :
                print("[CONSTANTE] HM_J est trop petit, essayez avec HM_J = " + str(HM_J+1) + " ou bien ré-évaluez HM_I et HASH_FACTOR.")
                return None
        res[j][k][0] = i
        res[j][k][1] = mime1_codes[i]
        res[j][k][2] = mime2_codes[i]

    return res