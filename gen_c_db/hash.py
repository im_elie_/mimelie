from constantes import *

def hash(e) :
    res = 0
    for u in e :
        if 48<=ord(u)<=57 :
            k = ord(u) - 48
        elif 65<=ord(u)<=90 :
            k = ord(u) - 65 + 10
        elif 97<=ord(u)<=122 :
            k = ord(u) - 97 + 36
        else :
            k = 63
        res += k
        res = (res * HASH_FACTOR)
    return res % HM_I