def compress_db(mime0, mime1, mime2) :
    # mime0
    for i in range(len(mime0)) :
        if mime0.index(mime0[i]) != i :
            print("L'extension " + mime0[i] + " n'est pas unique.")
    # mime1
    mime1_compressed = []
    mime1_codes = []
    for i in range(len(mime1)) :
        if mime1[i] in mime1_compressed :
            mime1_codes.append(mime1_compressed.index(mime1[i]))
        else :
            mime1_codes.append(len(mime1_compressed))
            mime1_compressed.append(mime1[i])
    # mime2
    mime2_compressed = []
    mime2_codes = []
    for i in range(len(mime2)) :
        if mime2[i] in mime2_compressed :
            mime2_codes.append(mime2_compressed.index(mime2[i]))
        else :
            mime2_codes.append(len(mime2_compressed))
            mime2_compressed.append(mime2[i])
    return (
        (mime0, [i for i in range(len(mime0))]),
        (mime1_compressed, mime1_codes),
        (mime2_compressed, mime2_codes)
        )

def decompress(i, compressed, codes) :
    return compressed[codes[i]]