from constantes import *
from import_mimelie_db import import_mimelie_db
from compress import compress_db
from dicgen import dicgen

# Calculs
mime0, mime1, mime2 = import_mimelie_db()
n = len(mime0)
(
(mime0_compressed, mime0_codes),
(mime1_compressed, mime1_codes),
(mime2_compressed, mime2_codes)
) = compress_db(mime0, mime1, mime2)
dic = dicgen(mime0, mime1_codes, mime2_codes)

# Sortie
print("#ifndef __MIMELIE_DB__\n#define __MIMELIE_DB__\n\n", end="")

print("#define MIME0 ", end="")
print("{", end="")
for i in range(len(mime0)) :
    print("\"" + mime0[i] + "\"", end="")
    if i + 1 != len(mime0) :
        print(", ", end="")
print("}", end="")

print("") # newline

print("#define MIME1 ", end="")
print("{", end="")
for i in range(len(mime1_compressed)) :
    print("\"" + mime1_compressed[i] + "\"", end="")
    if i + 1 != len(mime1_compressed) :
        print(", ", end="")
print("}", end="")

print("") # newline

print("#define MIME2 ", end="")
print("{", end="")
for i in range(len(mime2_compressed)) :
    print("\"" + mime2_compressed[i] + "\"", end="")
    if i + 1 != len(mime2_compressed) :
        print(", ", end="")
print("}", end="")

print("") # newline

print("#define MIME_HM ", end="")
print("{", end="")
for i in range(len(dic)) :
    print("{", end="")
    for j in range(len(dic[i])) :
        print("{", end="")
        for k in range(len(dic[i][j])) :
            print(dic[i][j][k], end="")
            if k + 1 != len(dic[i][j]) :
                print(", ", end="")
        print("}", end="")
        if j + 1 != len(dic[i]) :
            print(", ", end="")
    print("}", end="")
    if i + 1 != len(dic) :
        print(", ", end="")
print("}", end="")

print("") # newline

print("\n#endif /* __MIMELIE_DB__ */\n", end="")